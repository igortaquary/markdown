# COMPRASNET CONTRATOS

![Logo Comprasnet Contratos](https://gitlab.com/sistema-conta/sc/-/raw/master/public/img/logo_mail.png)

## Sistema de Gestão Administrativa para Órgãos Públicos

Sistema de Gestão Administrativa para Órgãos Públicos
O [Comprasnet Contratos][1] é uma ferramenta do governo federal que automatiza os processos de gestão contratual e conecta servidores públicos responsáveis pela execução e fiscalização de contratos, tornando informações disponíveis a qualquer momento e melhorando as condições de gestão e relacionamento com fornecedores.

## Conteúdo

- [Apresentação](apresentação.md)
- [Como acessar o sistema](acesso.md)



[1]: (https://contratos.comprasnet.gov.br/login) "Comprasnet Contratos"