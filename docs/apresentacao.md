## Apresentação

Com foco na busca pela eficiência da gestão e na melhoria do desempenho das instituições públicas, o Ministério da Economia, em parceria com a Advocacia-Geral da União, oferta aos órgãos e entidades da administração pública direta, autárquica e fundacional, incluindo as empresas estatais, o [Comprasnet Contratos][1]. A ferramenta faz parte das medidas de eficiência organizacional para o aprimoramento da administração pública federal direta, autárquica e fundacional estabelecidas pelo Decreto nº 9.739, de 28 de março de 2019 (Art. 6º, IX).
O [Comprasnet Contratos][1] é uma ferramenta do governo federal que automatiza os processos de gestão contratual e conecta servidores públicos responsáveis pela execução e fiscalização de contratos, tornando informações disponíveis a qualquer momento e melhorando as condições de gestão e relacionamento com fornecedores.

### Quem pode utilizar
- Órgãos e entidades da administração pública federal direta, autárquica e fundacional, bem como as empresas estatais; e 
- Demais órgãos e entidades de outros poderes ou das esferas estadual e municipal.

### Quanto custa
- O sistema é ofertado gratuitamente aos órgãos e entidades integrantes do Sistema Integrado de Serviços Gerais ([SISG](https://www.gov.br/compras/pt-br/acesso-a-informacao/institucional/sisg)), custeado pelo Ministério da Economia.

### Modelo de oferta do sistema
- Disponibilizado de forma centralizada, evitando custos com hospedagem e manutenção de sistemas de TIC hospedagem e manutenção de sistemas de TIC.

### Vantagens da plataforma
- Reduz os problemas relacionados às rotinas de trabalho;
- Pleno controle das informações do que acontece no âmbito dos contratos de um órgão ou entidade;
- Promove a eficiência na gestão contratual;
- Proporciona informações para apoiar as decisões governamentais de alocação mais eficiente de recursos;
- Infraestrutura centralizada, sem custos para órgãos e entidades do Poder Executivo federal;
- Maior transparência das informações dos contratos celebrados por toda a administração pública, permitindo a padronização de rotinas e procedimentos.

### A ferramenta viabiliza
- Controle de documentos diversos;
- Controle sobre os prazos de vigência dos contratos;
- Gestão sobre as informações financeiras do contrato;
- Visão global das penalidades aplicadas aos contratados;
- Controle sobre o valor desembolsado em cada contrato e sobre todos os contratos do órgão ou entidade;
- Gerenciamento dos diversos contratos sob a responsabilidade do gestor;
- Facilidade e praticidade nas sub-rogações;
- Padronização das ações de fiscalização por parte dos fiscais;
-	Controle dos atos administrativos praticados;
-	Controle sobre a fiscalização realizada;
-	Contato fácil com os fornecedores e solução rápida de impasses;
-	Controle sobre a realização de aditivos contratuais.


[1]: (https://contratos.comprasnet.gov.br/login) "Comprasnet Contratos"