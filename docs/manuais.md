## Manuais do sistema

Manuais\
Lista de Manuais

[Página de manuais](manuais)

(verificar)  
Apropriação Folha    
2.1. Preparação do arquivo DDP\
2.2. Listar Apropriações\
2.3. Importar DDP\
2.4. Identificação das Situações\
2.5. Identificação dos Empenhos e Valores\
2.6. Consulta saldo dos Empenhos no SIAFI\
2.7. Dados Básicos do Documento Folha\
2.8. Persistir Dados no Banco de Dados\
2.9. Apropriar no SIAFI